﻿using SharpDX.Direct3D11;

namespace SharkSpirit.RenderEngine.DepthStencil
{
    public static class DepthStencilDescriptionFactory
    {
        public static DepthStencilStateDescription CreateDepthStencilStateDescription()
        {
            var desc = new DepthStencilStateDescription
            {
                IsDepthEnabled = true,
                DepthWriteMask = DepthWriteMask.All,
                DepthComparison = Comparison.Less
            };

            return desc;
        }
    }
}
