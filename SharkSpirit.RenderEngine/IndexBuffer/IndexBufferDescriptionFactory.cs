﻿using SharpDX;
using SharpDX.Direct3D11;

namespace SharkSpirit.RenderEngine.IndexBuffer
{
    public static class IndexBufferDescriptionFactory
    {
        public static BufferDescription CreateIndexBufferDescription(int count)
        {
            var description = new BufferDescription()
            {
                Usage = ResourceUsage.Default,
                SizeInBytes = Utilities.SizeOf<ushort>() * count,
                BindFlags =BindFlags.IndexBuffer,
                CpuAccessFlags = CpuAccessFlags.None,
                OptionFlags = ResourceOptionFlags.None
            };

            return description;
        }
    }
}
