﻿using System;
using SharpDX.DXGI;

namespace SharkSpirit.RenderEngine.SwapChain
{
    public static class SwapChainDescriptionFactory
    {
        public static SwapChainDescription CreateSwapChainDescription()
        {
            var swapChainDescription = new SwapChainDescription
            {
                BufferCount = 2,
                Flags = SwapChainFlags.None,
                IsWindowed = true,
                ModeDescription = new ModeDescription(0, 0, new Rational(0, 0), Format.B8G8R8A8_UNorm),
                SampleDescription = new SampleDescription(1, 0),
                SwapEffect = SwapEffect.Discard,
                Usage = Usage.RenderTargetOutput,
            };

            return swapChainDescription;
        }

        public static SwapChainDescription CreateSwapChainDescription(IntPtr windowHandle)
        {
            var swapChainDescription = new SwapChainDescription
            {
                OutputHandle = windowHandle,
                BufferCount = 2,
                Flags = SwapChainFlags.None,
                IsWindowed = true,
                ModeDescription = new ModeDescription(0, 0, new Rational(60, 1), Format.B8G8R8A8_UNorm),
                SampleDescription = new SampleDescription(1, 0),
                SwapEffect = SwapEffect.Discard,
                Usage = Usage.RenderTargetOutput
            };

            return swapChainDescription;
        }
    }
}
