﻿using SharkSpirit.RenderEngine.Managers.GraphicsManager;
using SharpDX.D3DCompiler;

namespace SharkSpirit.RenderEngine.Bindable.VertexShader
{
    internal class VertexShaderBindable : Bindable
    {
        private readonly SharpDX.Direct3D11.VertexShader _vertexShader;

        public VertexShaderBindable(IGraphicsManager graphicsManager, string path) : base(graphicsManager)
        {
            var vertexShaderByteCode = ShaderBytecode.CompileFromFile(path, "VS", "vs_4_0", ShaderFlags.Debug);

            _vertexShader = new SharpDX.Direct3D11.VertexShader(GraphicsManager.GetDevice(), vertexShaderByteCode);
        }

        public override void Bind()
        {
            GraphicsManager.GetDeviceContext().VertexShader.SetShader(_vertexShader, null, 0);
        }

        public override string GetBindableDescription()
        {
            return "Vertex shader bindable";
        }
    }
}
