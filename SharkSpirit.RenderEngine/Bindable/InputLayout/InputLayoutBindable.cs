﻿using SharkSpirit.RenderEngine.Managers.GraphicsManager;
using SharpDX.D3DCompiler;
using SharpDX.Direct3D11;
using SharpDX.DXGI;

namespace SharkSpirit.RenderEngine.Bindable.InputLayout
{
    internal class InputLayoutBindable : Bindable
    {
        private readonly SharpDX.Direct3D11.InputLayout _inputLayout;

        public InputLayoutBindable(
            IGraphicsManager graphicsManager,
            string path) : base(graphicsManager)
        {
            var vertexShaderByteCode = ShaderBytecode.CompileFromFile(path, "VS", "vs_4_0", ShaderFlags.Debug);

            var signature = ShaderSignature.GetInputSignature(vertexShaderByteCode);

            _inputLayout = new SharpDX.Direct3D11.InputLayout(graphicsManager.GetDevice(), signature, new[]
            {
                new InputElement("POSITION", 0, Format.R32G32B32_Float, 0, 0),
                new InputElement("COLOR", 0, Format.R32G32B32A32_Float, 12, 0)
            });
        }

        public override void Bind()
        {
            GraphicsManager.GetDeviceContext().InputAssembler.InputLayout = _inputLayout;
        }

        public override string GetBindableDescription()
        {
            return "Input layout bindable";
        }
    }
}
