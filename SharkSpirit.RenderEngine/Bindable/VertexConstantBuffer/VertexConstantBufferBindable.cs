﻿using SharkSpirit.RenderEngine.Managers.GraphicsManager;

namespace SharkSpirit.RenderEngine.Bindable.VertexConstantBuffer
{
    internal class VertexConstantBufferBindable<T> : ConstantBuffer.ConstantBufferBindable<T> where T : struct
    {
        public VertexConstantBufferBindable(IGraphicsManager graphicsManager) : base(graphicsManager)
        {
        }

        public override void Bind()
        {
            GraphicsManager.GetDeviceContext().VertexShader.SetConstantBuffer(0, ConstantBuffer);
        }
    }
}
