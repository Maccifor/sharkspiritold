﻿using System;
using System.Runtime.InteropServices.WindowsRuntime;
using SharkSpirit.RenderEngine.Managers.GraphicsManager;

namespace SharkSpirit.RenderEngine.Bindable.PixelConstantBuffer
{
    internal class PixelConstantBufferBindable<T> : ConstantBuffer.ConstantBufferBindable<T> where T : struct
    {
        public PixelConstantBufferBindable(IGraphicsManager graphicsManager, T consts) : base(graphicsManager, consts)
        {
        }

        public override void Bind()
        {
            GraphicsManager.GetDeviceContext().PixelShader.SetConstantBuffer(0, ConstantBuffer);
        }
    }
}
