﻿using System;
using System.Runtime.InteropServices;
using SharkSpirit.RenderEngine.ConstantBuffer;
using SharkSpirit.RenderEngine.Managers.GraphicsManager;
using SharpDX;
using Buffer = SharpDX.Direct3D11.Buffer;

namespace SharkSpirit.RenderEngine.Bindable.ConstantBuffer
{
    [StructLayout(LayoutKind.Sequential)]
    struct ConstantBuffer
    {
        public Matrix World;
        public Matrix View;
        public Matrix Projection;
    }

    internal class ConstantBufferBindable<T> : Bindable where T : struct
    {
        public ConstantBufferBindable(IGraphicsManager graphicsManager) : base(graphicsManager)
        {
            var cbd = ConstantBufferDescriptionFactory.CreateConstantBufferDescription();

            ConstantBuffer = new Buffer(graphicsManager.GetDevice(), cbd);
        }

        public ConstantBufferBindable(IGraphicsManager graphicsManager, T consts) : base(graphicsManager)
        {
            var cbd = ConstantBufferDescriptionFactory.CreateConstantBufferDescription();

            ConstantBuffer = new Buffer(graphicsManager.GetDevice(), cbd);
        }

        protected  Buffer ConstantBuffer { get; private set; }

        public virtual void Update(T consts)
        {
            GraphicsManager.GetDeviceContext().UpdateSubresource(ref consts, ConstantBuffer);
        }

        public virtual void Update(ConstantBuffer consts, Buffer constantBuffer)
        {
            ConstantBuffer = constantBuffer;

            GraphicsManager.GetDeviceContext().UpdateSubresource(ref consts, constantBuffer);
        }

        public override void Bind()
        {
            
        }

        public override string GetBindableDescription()
        {
            return "Constant buffer bindable";
        }
    }
}
