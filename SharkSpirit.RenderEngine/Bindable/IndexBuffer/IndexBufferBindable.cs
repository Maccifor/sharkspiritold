﻿using SharkSpirit.RenderEngine.IndexBuffer;
using SharkSpirit.RenderEngine.Managers.GraphicsManager;
using SharpDX.Direct3D11;
using SharpDX.DXGI;

namespace SharkSpirit.RenderEngine.Bindable.IndexBuffer
{
    internal class IndexBufferBindable : Bindable
    {
        private readonly Buffer _indexBuffer;
        private readonly int _count;
        public IndexBufferBindable(
            IGraphicsManager graphicsManager,
            ushort[] indices) : base(graphicsManager)
        {
            var desc = IndexBufferDescriptionFactory.CreateIndexBufferDescription(indices.Length);

            _count = indices.Length;

            _indexBuffer = Buffer.Create(graphicsManager.GetDevice(), indices, desc);
        }

        public override void Bind()
        {
            GraphicsManager.GetDeviceContext().InputAssembler.SetIndexBuffer(_indexBuffer, Format.R16_UInt, 0);
        }

        public override string GetBindableDescription()
        {
            return "Index buffer bindable";
        }

        public override int GetCount()
        {
            return _count;
        }
    }
}
