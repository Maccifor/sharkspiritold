﻿using SharkSpirit.RenderEngine.Bindable.VertexConstantBuffer;
using SharkSpirit.RenderEngine.Drawable;
using SharkSpirit.RenderEngine.Managers.GraphicsManager;
using SharpDX;

namespace SharkSpirit.RenderEngine.Bindable.TransformConstantBuffer
{
    internal class TransformConstantBufferBindable : Bindable
    {
        private readonly VertexConstantBufferBindable<Matrix> _vertexConstantBuffer;

        public TransformConstantBufferBindable(IGraphicsManager graphicsManager, DrawableBase drawableBase) : base(graphicsManager)
        {
            _vertexConstantBuffer = new VertexConstantBufferBindable<Matrix>(graphicsManager);
        }

        public override void Bind()
        {
            var viewProjection = Matrix.Multiply(GraphicsManager.GetCamera().GetView(), GraphicsManager.GetProjection());
            var view = GraphicsManager.GetView();
            var world = GraphicsManager.GetWorld();

            var cb = new ConstantBuffer.ConstantBuffer()
            {
                World = Matrix.Transpose(world),
                View = Matrix.Transpose(view),
                Projection = Matrix.Transpose(viewProjection),
            };

            _vertexConstantBuffer.Update(cb, GraphicsManager.GetBuffer());

            _vertexConstantBuffer.Bind();
        }

        public override string GetBindableDescription()
        {
            return "Transform constant buffer bindable";
        }
    }
}
