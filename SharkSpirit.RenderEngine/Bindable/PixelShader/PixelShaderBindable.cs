﻿using SharkSpirit.RenderEngine.Managers.GraphicsManager;
using SharpDX.D3DCompiler;

namespace SharkSpirit.RenderEngine.Bindable.PixelShader
{
    internal class PixelShaderBindable : Bindable
    {
        private readonly SharpDX.Direct3D11.PixelShader _pixelShader;

        public PixelShaderBindable(IGraphicsManager graphicsManager, string path) : base(graphicsManager)
        {
            var pixelShaderByteCode = ShaderBytecode.CompileFromFile(path, "PS", "ps_4_0", ShaderFlags.Debug);

            _pixelShader = new SharpDX.Direct3D11.PixelShader(GraphicsManager.GetDevice(), pixelShaderByteCode);
        }

        public override void Bind()
        {
            GraphicsManager.GetDeviceContext().PixelShader.SetShader(_pixelShader, null, 0);
        }

        public override string GetBindableDescription()
        {
            return "Pixel shader bindable";
        }
    }
}
