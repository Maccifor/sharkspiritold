﻿using SharkSpirit.RenderEngine.Managers.GraphicsManager;
using SharpDX.Direct3D;

namespace SharkSpirit.RenderEngine.Bindable.Topology
{
    internal class TopologyBindable : Bindable
    {
        private readonly PrimitiveTopology _primitiveTopology;

        public TopologyBindable(
            IGraphicsManager graphicsManager, 
            PrimitiveTopology primitiveTopology) : base(graphicsManager)
        {
            _primitiveTopology = primitiveTopology;
        }

        public override void Bind()
        {
            GraphicsManager.GetDeviceContext().InputAssembler.PrimitiveTopology = _primitiveTopology;
        }

        public override string GetBindableDescription()
        {
            return "Topology bindable";
        }
    }
}
