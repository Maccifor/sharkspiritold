﻿using SharkSpirit.RenderEngine.Managers.GraphicsManager;

namespace SharkSpirit.RenderEngine.Bindable
{
    internal abstract class Bindable
    {
        protected Bindable(IGraphicsManager graphicsManager)
        {
            GraphicsManager = graphicsManager;
        }

        protected IGraphicsManager GraphicsManager { get; }

        public abstract void Bind();

        public abstract string GetBindableDescription();

        public virtual int GetCount()
        {
            return 0;
        }
    }
}
