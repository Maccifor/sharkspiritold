﻿using SharkSpirit.RenderEngine.Bindable.ConstantBuffer;
using SharkSpirit.RenderEngine.Managers.GraphicsManager;
using SharkSpirit.RenderEngine.VertexBuffer;
using SharpDX;
using SharpDX.Direct3D11;

namespace SharkSpirit.RenderEngine.Bindable.VertexBuffer
{
    internal class VertexBufferBindable<T> : ConstantBufferBindable<T> where T : unmanaged
    {
        private readonly Buffer _vertexBuffer;

        public VertexBufferBindable(IGraphicsManager graphicsManager, T[] vertices) : base(graphicsManager)
        {
            var vbd = VertexBufferDescriptionFactory.CreateVertexBufferDescription<T>(vertices.Length);

            _vertexBuffer = Buffer.Create(graphicsManager.GetDevice(), vertices, vbd);
        }

        public override void Bind()
        {
            GraphicsManager.GetDeviceContext().InputAssembler.SetVertexBuffers(0, new VertexBufferBinding(_vertexBuffer, Utilities.SizeOf<T>(), 0));
        }
    }
}
