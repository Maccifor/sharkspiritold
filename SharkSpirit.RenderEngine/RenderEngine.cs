﻿using System;
using System.Collections.Generic;
using SharkSpirit.Common;
using SharkSpirit.Common.Configuration;
using SharkSpirit.RenderEngine.Managers.GraphicsManager;
using SharkSpirit.RenderEngine.Managers.LightManager;

namespace SharkSpirit.RenderEngine
{
    public class RenderEngine : IRenderEngine
    {
        private readonly SystemConfiguration _systemConfiguration;

        public RenderEngine(SystemConfiguration systemConfiguration)
        {
            _systemConfiguration = systemConfiguration;

            GraphicsManager = new GraphicsManager();
            LightManager = new LightManager();
        }

        internal IGraphicsManager GraphicsManager { get; }
        internal ILightManager LightManager { get; }

        public void Initialize()
        {
            
        }

        public void Initialize(IntPtr windowHandle)
        {
            Console.WriteLine("Initialize graphics manager");
            GraphicsManager.Initialize(windowHandle, _systemConfiguration);

            Console.WriteLine("Initialize light manager");
            LightManager.Initialize();
        }

        public void Render(IEnumerable<IDrawable> drawables)
        {
            GraphicsManager.ClearBuffer();
            GraphicsManager.Draw(drawables);
        }

        public void NewSurface(IntPtr windowHandle, SystemConfiguration systemConfiguration)
        {
            Console.WriteLine($"Init new surface {windowHandle}");

            GraphicsManager.InitNewSurface(windowHandle, systemConfiguration);
        }

        public IGraphicsManager GetGraphicsManager()
        {
            return GraphicsManager;
        }
    }
}
