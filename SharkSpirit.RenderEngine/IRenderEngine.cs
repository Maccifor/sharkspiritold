﻿using System;
using System.Collections.Generic;
using SharkSpirit.Common;
using SharkSpirit.Common.Configuration;
using SharkSpirit.RenderEngine.Managers.GraphicsManager;

namespace SharkSpirit.RenderEngine
{
    public interface IRenderEngine
    {
        void Initialize();

        void Initialize(IntPtr windowHandle);

        void Render(IEnumerable<IDrawable> drawables);

        void NewSurface(IntPtr windowHandle, SystemConfiguration systemConfiguration);

        IGraphicsManager GetGraphicsManager();
    }
}