﻿using SharkSpirit.Common;
using SharkSpirit.Common.Configuration;
using SharkSpirit.RenderEngine.Drawable;
using SharkSpirit.RenderEngine.Managers.GraphicsManager;

namespace SharkSpirit.RenderEngine.DrawableFactories
{
    public static class BoxFactory
    {
        public static IDrawable CreateBox(IGraphicsManager graphicsManager, SystemConfiguration systemConfiguration)
        {
            var box = new Box(graphicsManager, systemConfiguration);

            return box;
        }
    }
}
