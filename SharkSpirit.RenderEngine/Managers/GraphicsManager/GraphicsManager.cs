﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using SharkSpirit.Common;
using SharkSpirit.Common.Configuration;
using SharpDX;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using Buffer = SharpDX.Direct3D11.Buffer;
using Device = SharpDX.Direct3D11.Device;

namespace SharkSpirit.RenderEngine.Managers.GraphicsManager
{
    public class GraphicsManager : IGraphicsManager
    {
        private Device _device;
        private DeviceContext _deviceContext;
        private SharpDX.Direct3D11.RenderTargetView _renderTargetView;
        private IntPtr _windowHandle;
        private Matrix _projection;
        private FpsManager _fpsManager;
        private CameraManager _cameraManager;
        private Matrix _view;
        private Matrix _world;
        private Buffer _constantBuffer;
        private SystemConfiguration _systemConfiguration;
        public void ClearBuffer()
        {
            _deviceContext.ClearRenderTargetView(_renderTargetView, new Color4(0.07f, 0.0f, 0.12f, 1.0f));
        }

        public void ClearBuffer(float red, float green, float blue)
        {
            _deviceContext.ClearRenderTargetView(_renderTargetView, new Color4(red, green, blue, 1.0f));
        }


        public void Draw(IEnumerable<IDrawable> drawables)
        {
            _fpsManager.Tick();

            _cameraManager.Render();

            foreach (var drawable in drawables)
            {
                drawable.Draw();
            }

            _deviceContext?.Flush();
        }

        public IntPtr GetWindowHandle()
        {
            return _windowHandle;
        }

        public void Dispose()
        {
            _deviceContext?.Dispose();
            _renderTargetView?.Dispose();
            _device?.Dispose();
        }

        public void InitNewSurface(IntPtr windowHandle, SystemConfiguration systemConfiguration)
        {
            _systemConfiguration = systemConfiguration;

            var dxgiResourceTypeGuid = Marshal.GenerateGuidForType(typeof(SharpDX.DXGI.Resource));
            Marshal.QueryInterface(windowHandle, ref dxgiResourceTypeGuid, out var dxgiResource);
            var dxgiObject = new SharpDX.DXGI.Resource(dxgiResource);
            var sharedHandle = dxgiObject.SharedHandle;
            var outputResource = _device.OpenSharedResource<SharpDX.Direct3D11.Texture2D>(sharedHandle);
            dxgiObject.Dispose();
            var rtDesc = new RenderTargetViewDescription
            {
                Format = Format.B8G8R8A8_UNorm,
                Dimension = RenderTargetViewDimension.Texture2D,
                Texture2D = {MipSlice = 0}
            };

            _renderTargetView = new SharpDX.Direct3D11.RenderTargetView(_device, outputResource, rtDesc);

            var outputResourceDesc = outputResource.Description;
            if (outputResourceDesc.Width != _systemConfiguration.GraphicsConfigurationInfo.Width || outputResourceDesc.Height != _systemConfiguration.GraphicsConfigurationInfo.Height)
            {
                SetUpViewport();
            }
            _deviceContext.OutputMerger.SetRenderTargets(null, _renderTargetView);
            outputResource.Dispose();

            _windowHandle = windowHandle;
        }

        public Device GetDevice() => _device;
        public DeviceContext GetDeviceContext() => _deviceContext;
        public Matrix GetProjection() => _projection;

        public void SetProjection(Matrix projection) => _projection = projection;
        public CameraManager GetCamera()
        {
            return _cameraManager;
        }

        public Matrix GetView()
        {
            return _view;
        }

        public Matrix GetWorld()
        {
            return _world;
        }

        public Buffer GetBuffer()
        {
            return _constantBuffer;
        }

        private void SetUpViewport()
        {
            var vp = new Viewport(0, 0, (int)_systemConfiguration.GraphicsConfigurationInfo.Width, (int)_systemConfiguration.GraphicsConfigurationInfo.Height, 0f, 1f);
            _deviceContext.Rasterizer.SetViewport(vp);
            _projection = Matrix.PerspectiveFovLH(MathUtil.PiOverFour, (float)_systemConfiguration.GraphicsConfigurationInfo.Width / (float)_systemConfiguration.GraphicsConfigurationInfo.Height, 0.01f, 100f);
        }

        public void Initialize()
        {

        }

        public void Initialize(IntPtr windowHandle, SystemConfiguration systemConfiguration)
        {
            _systemConfiguration = systemConfiguration;

            _fpsManager = new FpsManager(120);
            _cameraManager = new CameraManager();

            Console.WriteLine($"Window handle {windowHandle}");

            _windowHandle = windowHandle;

            var createDeviceFlag = DeviceCreationFlags.BgraSupport;
            var driverTypes = new[] { DriverType.Hardware, DriverType.Warp, DriverType.Reference };
            var featureLevels = new[] { FeatureLevel.Level_11_0, FeatureLevel.Level_10_1, FeatureLevel.Level_10_0 };
            foreach (var dt in driverTypes)
            {
                try
                {
                    _device = new Device(dt, createDeviceFlag, featureLevels);
                }
                catch (Exception e)
                {
                    continue;
                }
                break;
            }

            _deviceContext = _device.ImmediateContext;

            Console.WriteLine("Setting render targets");
            _deviceContext.OutputMerger.SetRenderTargets(null, _renderTargetView);

            Console.WriteLine("Set up viewport");
            SetUpViewport();

            var cbd = new BufferDescription()
            {
                Usage = ResourceUsage.Default,
                SizeInBytes = Utilities.SizeOf<Bindable.ConstantBuffer.ConstantBuffer>(),
                BindFlags = BindFlags.ConstantBuffer,
                CpuAccessFlags = CpuAccessFlags.None
            };
            _constantBuffer = new Buffer(_device, cbd);

            _world = Matrix.Identity;

            var eye = new Vector3(0, 1, -5);
            var at = new Vector3(0, 1, 0);
            var up = new Vector3(0, 1, 0);
            _view = Matrix.LookAtLH(eye, at, up);
        }
    }
}
