﻿using System;

namespace SharkSpirit.RenderEngine.Managers.GraphicsManager
{
    public class FpsManager
    {
        private double _timePerTick;
        private long _amountOfTicks;
        private double _nextTime;
        private int _updates;
        private DateTime _time;
        public FpsManager(long amountOfTicks)
        {
            _time = DateTime.Now;
            _amountOfTicks = amountOfTicks;
            _updates = 0;
            _timePerTick = (double)TimeSpan.TicksPerSecond / amountOfTicks;
            _nextTime = DateTime.Now.Ticks;
        }

        public void Tick()
        {
            if ((_nextTime - DateTime.Now.Ticks) <= 0)
            {
                _nextTime += _timePerTick;
                Tick();
                _updates++;
            }
            if ((DateTime.Now - _time).Ticks > 10000000)
            {
                _time += new TimeSpan(TimeSpan.TicksPerSecond);
                Console.WriteLine(_updates + " Ticks");
                _updates = 0;
            }
        }
    }
}
