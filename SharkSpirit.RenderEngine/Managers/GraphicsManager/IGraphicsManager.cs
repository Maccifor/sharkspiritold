﻿using System;
using System.Collections.Generic;
using SharkSpirit.Common;
using SharkSpirit.Common.Configuration;
using SharpDX;
using SharpDX.Direct3D11;
using Buffer = SharpDX.Direct3D11.Buffer;
using Device = SharpDX.Direct3D11.Device;

namespace SharkSpirit.RenderEngine.Managers.GraphicsManager
{
    public interface IGraphicsManager : IManager
    {
        void ClearBuffer();

        void ClearBuffer(float red, float green, float blue);

        void Draw(IEnumerable<IDrawable> drawables);

        IntPtr GetWindowHandle();

        void Dispose();

        void InitNewSurface(IntPtr windowHandle, SystemConfiguration systemConfiguration);

        Device GetDevice();

        DeviceContext GetDeviceContext();

        Matrix GetProjection();

        void SetProjection(Matrix projection);
        CameraManager GetCamera();
        Matrix GetView();
        Matrix GetWorld();
        Buffer GetBuffer();
    }
}
