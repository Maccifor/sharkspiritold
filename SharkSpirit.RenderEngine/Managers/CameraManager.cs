﻿using SharpDX;

namespace SharkSpirit.RenderEngine.Managers
{
    public class CameraManager
    {
        private float PositionX { get; set; }
        private float PositionY { get; set; }
        private float PositionZ { get; set; }
        private float RotationX { get; set; }
        private float RotationY { get; set; }
        private float RotationZ { get; set; }
        public Matrix ViewMatrix { get; private set; }

        public void SetXPosition(float x)
        {
            PositionX = x;
        }

        public void SetYPosition(float y)
        {
            PositionY = y;
        }

        public void SetZPosition(float z)
        {
            PositionZ = z;
        }

        public void Render()
        {
            var position = new Vector3(PositionX, PositionY, PositionZ);

            var lookAt = new Vector3(0, 0, 1);

            var pitch = RotationX * 0.0174532925f;
            var yaw = RotationY * 0.0174532925f; 
            var roll = RotationZ * 0.0174532925f; 

            var rotationMatrix = Matrix.RotationYawPitchRoll(yaw, pitch, roll);

            lookAt = Vector3.TransformCoordinate(lookAt, rotationMatrix);
            var up = Vector3.TransformCoordinate(Vector3.UnitY, rotationMatrix);

            lookAt = position + lookAt;

            ViewMatrix = Matrix.LookAtLH(position, lookAt, up);
        }

        public Matrix GetView()
        {
            return ViewMatrix;
        }

        public float GetXPosition()
        {
            return PositionX;
        }

        public float GetYPosition()
        {
            return PositionY;
        }

        public float GetZPosition()
        {
            return PositionZ;
        }

        public void SetRotationX(int x)
        {
            RotationY = x;
        }

        public void SetRotationY(int y)
        {
            RotationX = y;
        }

        public void SetRotationZ(int z)
        {
            RotationZ = z;
        }
    }
}
