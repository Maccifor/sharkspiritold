﻿using System.IO;
using SharpDX.D3DCompiler;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using Device = SharpDX.Direct3D11.Device;

namespace SharkSpirit.RenderEngine.Managers.ShadersManager
{
    internal class ShadersManager : IShadersManager
    {
        public ShaderInfo LoadShaders(Device device)
        {
            var path = "C:\\Repositories\\BitBucket\\SharkSpirit\\SharkSpirit.Graphics\\Shaders";

            var vertexPath = Path.Combine(path, "vertexShader.hlsl");

            var vertexShaderByteCode = ShaderBytecode.CompileFromFile(vertexPath, "main", "vs_4_0", ShaderFlags.Debug);

            var vertexShader = new VertexShader(device, vertexShaderByteCode);

            var pixelPath = Path.Combine(path, "pixelShader.hlsl");

            var pixelShaderByteCode = ShaderBytecode.CompileFromFile(pixelPath, "main", "ps_4_0", ShaderFlags.Debug);

            var pixelShader = new PixelShader(device, pixelShaderByteCode);

            var signature = ShaderSignature.GetInputSignature(vertexShaderByteCode);

            var inputLayout = new InputLayout(device, signature, new[]
            {
                new InputElement("POSITION", 0, Format.R32G32B32_Float, 0, 0),
                new InputElement("COLOR", 0, Format.R32G32B32A32_Float, 12, 0)
            });

            var shaderInfo = new ShaderInfo(vertexShader, pixelShader, inputLayout);

            pixelShaderByteCode.Dispose();
            vertexShaderByteCode.Dispose();

            return shaderInfo;
        }
    }

    internal interface IShadersManager
    {
        ShaderInfo LoadShaders(Device device);
    }


    internal class ShaderInfo
    {
        public ShaderInfo(VertexShader vertexShader, PixelShader pixelShader, InputLayout inputLayout)
        {
            VertexShader = vertexShader;
            PixelShader = pixelShader;
            InputLayout = inputLayout;
        }

        public VertexShader VertexShader { get; private set; }

        public PixelShader PixelShader { get; private set; }

        public InputLayout InputLayout { get; private set; }
    }
}
