﻿using System;
using SharkSpirit.Common.Configuration;

namespace SharkSpirit.RenderEngine.Managers
{
    public interface IManager
    {
        void Initialize();
        void Initialize(IntPtr intPtr, SystemConfiguration systemConfiguration);
    }
}
