﻿using SharpDX.Direct3D11;
using SharpDX.DXGI;

namespace SharkSpirit.RenderEngine.RenderTargetView
{
    public static class RenderTargetViewDescriptionFactory
    {

        public static RenderTargetViewDescription CreateRenderTargetViewDescription()
        {
            var rtDesc = new RenderTargetViewDescription
            {
                Format = Format.B8G8R8A8_UNorm,
                Dimension = RenderTargetViewDimension.Texture2D,
                Texture2D = { MipSlice = 0 }
            };

            return rtDesc;
        }
    }
}
