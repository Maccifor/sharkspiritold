﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using SharkSpirit.Common;
using SharkSpirit.RenderEngine.Bindable.IndexBuffer;
using SharkSpirit.RenderEngine.Managers.GraphicsManager;
using SharpDX;

namespace SharkSpirit.RenderEngine.Drawable
{
    internal class DrawableBase : IDrawable
    {
        private readonly List<Bindable.Bindable> _bindables;

        public DrawableBase(IGraphicsManager graphicsManager)
        {
            _bindables = new List<Bindable.Bindable>();
            GraphicsManager = graphicsManager;
        }

        protected IndexBufferBindable IndexBufferBindable;
        protected IGraphicsManager GraphicsManager;

        protected void AddBindable(Bindable.Bindable bindable)
        {
            _bindables.Add(bindable);
        }

        protected void AddIndexBuffer(IndexBufferBindable indexBufferBindable)
        {
            IndexBufferBindable = indexBufferBindable;

            _bindables.Add(indexBufferBindable);
        }

        public virtual Matrix GetTransformXm()
        {
            return Matrix.Zero;
        }

        public void Draw()
        {
            foreach (var bindable in _bindables)
            {
                bindable.Bind();
            }

            GraphicsManager.GetDeviceContext().DrawIndexed(IndexBufferBindable.GetCount(), 0, 0);
        }
    }
}
