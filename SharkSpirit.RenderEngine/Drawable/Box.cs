﻿using System.IO;
using SharkSpirit.Common.Configuration;
using SharkSpirit.RenderEngine.Bindable.IndexBuffer;
using SharkSpirit.RenderEngine.Bindable.InputLayout;
using SharkSpirit.RenderEngine.Bindable.PixelShader;
using SharkSpirit.RenderEngine.Bindable.Topology;
using SharkSpirit.RenderEngine.Bindable.TransformConstantBuffer;
using SharkSpirit.RenderEngine.Bindable.VertexBuffer;
using SharkSpirit.RenderEngine.Bindable.VertexShader;
using SharkSpirit.RenderEngine.Managers.GraphicsManager;
using SharpDX;
using SharpDX.Direct3D;

namespace SharkSpirit.RenderEngine.Drawable
{
    internal class Box : DrawableBase
    {
       
        public Box(IGraphicsManager graphicsManager, SystemConfiguration systemConfiguration) : base(graphicsManager)
        {
            var vertices = new[]
            {
                new SimpleVertex(new Vector3(-1.0f, 1.0f, -1.0f), new Vector4(0.0f, 0.0f, 1.0f, 0.5f)),
                new SimpleVertex(new Vector3(1.0f, 1.0f, -1.0f), new Vector4(0.0f, 1.0f, 0.0f, 0.5f)),
                new SimpleVertex(new Vector3(1.0f, 1.0f, 1.0f), new Vector4(0.0f, 1.0f, 1.0f, 0.5f)),
                new SimpleVertex(new Vector3(-1.0f, 1.0f, 1.0f), new Vector4(1.0f, 0.0f, 0.0f, 0.5f)),
                new SimpleVertex(new Vector3(-1.0f, -1.0f, -1.0f), new Vector4(1.0f, 0.0f, 1.0f, 0.5f)),
                new SimpleVertex(new Vector3(1.0f, -1.0f, -1.0f), new Vector4(1.0f, 1.0f, 0.0f, 0.5f)),
                new SimpleVertex(new Vector3(1.0f, -1.0f, 1.0f), new Vector4(1.0f, 1.0f, 1.0f, 0.5f)),
                new SimpleVertex(new Vector3(-1.0f, -1.0f, 1.0f), new Vector4(0.0f, 0.0f, 0.0f, 0.5f)),
            };

            AddBindable(new VertexShaderBindable(graphicsManager, Path.Combine(systemConfiguration.GraphicsConfigurationInfo.ShaderFilePath, "vertexShader.hlsl")));

            AddBindable(new PixelShaderBindable(graphicsManager, Path.Combine(systemConfiguration.GraphicsConfigurationInfo.ShaderFilePath, "pixelShader.hlsl")));

            AddBindable(new VertexBufferBindable<SimpleVertex>(graphicsManager, vertices));

            var indices = new ushort[]
            {
                3, 1, 0,
                2, 1, 3,

                0, 5, 4,
                1, 5, 0,

                3, 4, 7,
                0, 4, 3,

                1, 6, 5,
                2, 6, 1,

                2, 7, 6,
                3, 7, 2,

                6, 4, 5,
                7, 4, 6,
            };


            AddIndexBuffer(new IndexBufferBindable(graphicsManager, indices));

            AddBindable(new InputLayoutBindable(graphicsManager, Path.Combine(systemConfiguration.GraphicsConfigurationInfo.ShaderFilePath, "vertexShader.hlsl")));

            AddBindable(new TopologyBindable(graphicsManager, PrimitiveTopology.TriangleList));

            AddBindable(new TransformConstantBufferBindable(graphicsManager, this));
        }
    }
}
