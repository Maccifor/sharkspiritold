﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using SharkSpirit.RenderEngine.Bindable.ConstantBuffer;
using SharpDX;
using SharpDX.Direct3D11;

namespace SharkSpirit.RenderEngine.ConstantBuffer
{
    public static class ConstantBufferDescriptionFactory
    {
        public static BufferDescription CreateConstantBufferDescription()
        {
            var cbd = new BufferDescription()
            {
                Usage = ResourceUsage.Default,
                SizeInBytes = Utilities.SizeOf<Bindable.ConstantBuffer.ConstantBuffer>(),
                BindFlags = BindFlags.ConstantBuffer,
                CpuAccessFlags = CpuAccessFlags.None,
            };

            return cbd;
        }
    }
}
