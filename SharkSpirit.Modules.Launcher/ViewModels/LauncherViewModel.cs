﻿using Prism.Events;
using SharkSpirit.Modules.Core.Application;
using SharkSpirit.Modules.Core.ViewModels;

namespace SharkSpirit.Modules.Launcher.ViewModels
{
    public class LauncherViewModel : ViewModelBase
    {
        private readonly IEventAggregator _eventAggregator;

        public LauncherViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;

        }

        public void InitAsync()
        {
            RaiseCompleteInitialization(ApplicationInitializationResult.Success);

        }

        private void RaiseCompleteInitialization(ApplicationInitializationResult result)
        {
            _eventAggregator.GetEvent<ApplicationInitializedEvent>().Publish(result);
        }
    }
}
