﻿using System;

namespace SharkSpirit.Core.Logging
{
    public class BaseLogger
    {
        private readonly ILog _logger;

        public BaseLogger(ILog logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Логирование пользовательского действия
        /// </summary>
        /// <param name="action"></param>
        public void LogUserAction(string action)
        {
            _logger.Info($"Action={action}");
        }

        /// <summary>
        /// Логирование программного действия
        /// </summary>
        /// <param name="action"></param>
        public void LogAutoAction(string action)
        {
            _logger.Info($"AutoAction={action}");
        }

        public void LogError(string message, Exception e)
        {
            _logger.ErrorException(message, e);
        }

        public void LogErrorMessage(string message)
        {
            _logger.Error(message);
        }

        public void LogWarning(string message)
        {
            _logger.Warn(message);
        }

        public void LogInfo(string message)
        {
            _logger.Info(message);
        }

        public void LogDebug(string message)
        {
            _logger.Debug(message);
        }
    }
}
