﻿using System;
using System.Collections.Generic;
using SharkSpirit.Common;
using SharkSpirit.Common.Configuration;
using SharkSpirit.RenderEngine.Managers.GraphicsManager;

namespace SharkSpirit.Core
{
    public interface IEngineCore
    {
        void InitializeRenderEngine();

        void InitializeRenderEngine(IntPtr windowHandle);

        void InitializePhysicsEngine();

        void InitializeAudioEngine();

        void Render(IEnumerable<IDrawable> sceneItems);

        void InitNewRenderSurface(IntPtr windowHandle, SystemConfiguration  systemConfiguration);

        IGraphicsManager GetGraphicsManager();
    }
}