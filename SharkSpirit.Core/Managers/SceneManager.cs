﻿using System;
using System.Collections.Generic;
using SharkSpirit.Common;
using SharkSpirit.Common.Configuration;
using SharkSpirit.Core.Logging;
using SharkSpirit.Core.Managers.ManagersInterfaces;
using SharkSpirit.RenderEngine.DrawableFactories;

namespace SharkSpirit.Core.Managers
{
    public class SceneManager : ISceneManager
    {
        public SceneManager(IntPtr windowHandle, SystemConfiguration systemConfiguration)
        {
            EngineCore = new EngineCore(systemConfiguration);
            Logger = new CoreLogger();
            SceneItems = new List<IDrawable>();

            Initialize(windowHandle, systemConfiguration);

            SceneItems.Add(BoxFactory.CreateBox(EngineCore.GetGraphicsManager(), systemConfiguration));
        }

        public IEngineCore EngineCore { get; }

        public CoreLogger Logger { get; }

        public List<IDrawable> SceneItems { get; }

        public void Draw()
        {
            try
            {
                EngineCore.Render(SceneItems);
            }
            catch (Exception e)
            {
                Logger.LogError("Error while rendering", e);
            }
        }

        public void InitNewSurface(IntPtr windowHandle, SystemConfiguration systemConfiguration)
        {
            try
            {
                Logger.LogInfo("Initialize new render surface");

                EngineCore.InitNewRenderSurface(windowHandle, systemConfiguration);
            }
            catch (Exception e)
            {
                Logger.LogError("Error while initializing new render surface", e);
            }
        }

        public void ChangeXPosition(int x)
        {
            var camera = EngineCore
                .GetGraphicsManager()
                .GetCamera();

            camera.SetXPosition(x);
        }

        public void ChangeYPosition(int y)
        {
            var camera = EngineCore
                .GetGraphicsManager()
                .GetCamera();

            camera.SetYPosition(y);
        }

        public void ChangeZPosition(int z)
        {
            var camera = EngineCore
                .GetGraphicsManager()
                .GetCamera();

            camera.SetZPosition(z);
        }

        public void ChangeRotationX(int x)
        {
            var camera = EngineCore
                .GetGraphicsManager()
                .GetCamera();

            camera.SetRotationX(x);
        }

        public void ChangeRotationY(int y)
        {
            var camera = EngineCore
                .GetGraphicsManager()
                .GetCamera();

            camera.SetRotationY(y);

        }

        public void ChangeRotationZ(int z)
        {
            var camera = EngineCore
                .GetGraphicsManager()
                .GetCamera();

            camera.SetRotationZ(z);

        }

        private void Initialize(IntPtr windowHandle, SystemConfiguration systemConfiguration)
        {
            try
            {
                Logger.LogInfo("Initialize render engine");
                EngineCore.InitializeRenderEngine(windowHandle);

                Logger.LogInfo("Initialize physics engine");
                EngineCore.InitializePhysicsEngine();

                Logger.LogInfo("Initialize audio engine");
                EngineCore.InitializeAudioEngine();
            }
            catch (Exception e)
            {
                Logger.LogError("Engine initializing error", e);
            }
        }
    }
}
