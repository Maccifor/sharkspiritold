﻿using System;
using SharkSpirit.Common.Configuration;

namespace SharkSpirit.Core.Managers.ManagersInterfaces
{
    public interface ISceneManager
    {
        void Draw();
        void InitNewSurface(IntPtr resourcePointer, SystemConfiguration systemConfiguration);
        void ChangeXPosition(int radius);
        void ChangeYPosition(int theta);
        void ChangeZPosition(int phi);
        void ChangeRotationX(int x);
        void ChangeRotationY(int y);
        void ChangeRotationZ(int z);
    }
}