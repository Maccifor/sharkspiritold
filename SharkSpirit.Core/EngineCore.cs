﻿using System;
using System.Collections.Generic;
using SharkSpirit.AudioEngine;
using SharkSpirit.Common;
using SharkSpirit.Common.Configuration;
using SharkSpirit.PhysicsEngine;
using SharkSpirit.RenderEngine;
using SharkSpirit.RenderEngine.Managers.GraphicsManager;

namespace SharkSpirit.Core
{
    public class EngineCore : IEngineCore
    {
        public EngineCore(Common.Configuration.SystemConfiguration systemConfiguration)
        {
            PhysicsEngine = new PhysicsEngine.PhysicsEngine();
            RenderEngine = new RenderEngine.RenderEngine(systemConfiguration);
            AudioEngine = new AudioEngine.AudioEngine();
        }

        /// <summary>
        /// Physics engine instance
        /// </summary>
        public IPhysicsEngine PhysicsEngine { get; }


        /// <summary>
        /// Render engine instance 
        /// </summary>
        public IRenderEngine RenderEngine { get; }


        /// <summary>
        /// Audio engine instance 
        /// </summary>
        public IAudioEngine AudioEngine { get; }


        /// <summary>
        /// Initialize render engine without handle to window
        /// </summary>
        public void InitializeRenderEngine()
        {
            
        }

        /// <summary>
        /// Initialize render engine with handle to window
        /// </summary>
        /// <param name="windowHandle">pointer to render window</param>
        public void InitializeRenderEngine(IntPtr windowHandle)
        {
            Console.WriteLine("Initialize render engine");
            RenderEngine.Initialize(windowHandle);
        }


        /// <summary>
        /// Initialize physics engine
        /// </summary>
        public void InitializePhysicsEngine()
        {
            Console.WriteLine("Initialize physics engine");
            PhysicsEngine.Initialize();
        }


        /// <summary>
        /// Initialize audio engine
        /// </summary>
        public void InitializeAudioEngine()
        {
            Console.WriteLine("Initialize audio engine");
            AudioEngine.Initialize();
        }

        /// <summary>
        /// Render frame
        /// </summary>
        /// <param name="sceneItems"></param>
        public void Render(IEnumerable<IDrawable> sceneItems)
        {
            RenderEngine.Render(sceneItems);
        }


        /// <summary>
        /// Initialize new surface for rendering
        /// </summary>
        /// <param name="windowHandle">pointer to render window</param>
        /// <param name="systemConfiguration"></param>
        public void InitNewRenderSurface(IntPtr windowHandle, SystemConfiguration systemConfiguration)
        {
            RenderEngine.NewSurface(windowHandle, systemConfiguration);
        }

        public IGraphicsManager GetGraphicsManager()
        {
            return RenderEngine.GetGraphicsManager();
        }
    }
}
