﻿namespace SharkSpirit.Common
{
    public interface IDrawable
    {
        void Draw();
    }
}
