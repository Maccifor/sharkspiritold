﻿namespace SharkSpirit.Common.Configuration
{
    public class SystemConfiguration
    {
        public SystemConfiguration(GraphicsConfigurationInfo graphicsConfigurationInfo)
        {
            GraphicsConfigurationInfo = graphicsConfigurationInfo;
        }

        public GraphicsConfigurationInfo GraphicsConfigurationInfo { get; private set; }
    }
}
