﻿namespace SharkSpirit.Common.Configuration
{
    public class GraphicsConfigurationInfo
    {
        public double Width { get; set; }
        public double Height { get; set; }
        public bool FullScreen { get; set; }
        public bool VerticalSyncEnabled { get; set; }
        public float ScreenDepth { get; set; }
        public float ScreenNear { get; set; }
        public string ShaderFilePath { get; set; }
    }
}
