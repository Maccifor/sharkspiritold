﻿using System.ComponentModel;
using System.Reactive;
using Prism.Events;
using ReactiveUI;
using SharkSpirit.Modules.Core.Application;
using SharkSpirit.Modules.Core.ViewModels;

namespace SharkSpirit.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        private readonly IEventAggregator _eventAggregator;

        public MainWindowViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;

            Closing = ReactiveCommand.Create<CancelEventArgs>(OnClosing);

            _eventAggregator.GetEvent<ApplicationInitializedEvent>().Publish(ApplicationInitializationResult.Success);
        }

        public ReactiveCommand<CancelEventArgs, Unit> Closing { get; }

        private void OnClosing(CancelEventArgs cancelEventArgs)
        {
            _eventAggregator.GetEvent<ApplicationInitializedEvent>().Publish(ApplicationInitializationResult.NeedClose);
        }
    }
}
