﻿using System;
using ReactiveUI;
using SharkSpirit.Common.Configuration;
using SharkSpirit.Core.Managers;
using SharkSpirit.Core.Managers.ManagersInterfaces;
using SharkSpirit.Modules.Core.ViewModels;

namespace SharkSpirit.Modules.Scene.ViewModels
{
    public class SceneViewModel : DockWindowViewModel
    {
        private SystemConfiguration _systemConfiguration;
        private readonly ISceneManager _sceneManager;

        public SceneViewModel(IntPtr windowHandle, SystemConfiguration systemConfiguration)
        {
            _systemConfiguration = systemConfiguration;
            CanClose = true;
            Title = "Engine";
            _sceneManager = new SceneManager(windowHandle, systemConfiguration);

            Radius = -8;
            Theta = 8;
            Phi = -13;
            RotationX = 24;
            RotationY = 24;
            RotationZ = 0;

            CloseCommand = ReactiveCommand.Create(() => { IsClosed = true; });

            this.WhenAnyValue(model => model.Radius)
                .Subscribe(ChangeRadius);

            this.WhenAnyValue(model => model.Theta)
                .Subscribe(ChangeTheta);

            this.WhenAnyValue(model => model.Phi)
                .Subscribe(ChangePhi);

            this.WhenAnyValue(model => model.RotationX)
                .Subscribe(ChangeRotationX);

            this.WhenAnyValue(model => model.RotationY)
                .Subscribe(ChangeRotationY);

            this.WhenAnyValue(model => model.RotationZ)
                .Subscribe(ChangeRotationZ);
        }

        private void ChangeRadius(int radius)
        {
            _sceneManager.ChangeXPosition(radius);
        }

        private void ChangeTheta(int theta)
        {
            _sceneManager.ChangeYPosition(theta);
        }

        private void ChangePhi(int phi)
        {
            _sceneManager.ChangeZPosition(phi);
        }

        private void ChangeRotationX(int x)
        {
            _sceneManager.ChangeRotationX(x);
        }

        private void ChangeRotationY(int y)
        {
            _sceneManager.ChangeRotationY(y);
        }

        private void ChangeRotationZ(int z)
        {
            _sceneManager.ChangeRotationZ(z);
        }

        public void OnRender(IntPtr resourcePointer, bool isNewSurface)
        {
            if (isNewSurface)
            {
                _sceneManager.InitNewSurface(resourcePointer, _systemConfiguration);
            }

            _sceneManager.Draw();
        }

        public void SetNewConfiguration(SystemConfiguration sysConfig)
        {
            _systemConfiguration = sysConfig;
        }
    }
}
