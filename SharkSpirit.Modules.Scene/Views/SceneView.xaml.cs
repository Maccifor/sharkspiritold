﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using SharkSpirit.Common.Configuration;
using SharkSpirit.Modules.Core.AvalonDock;
using SharkSpirit.Modules.Scene.ViewModels;

namespace SharkSpirit.Modules.Scene.Views
{
    /// <summary>
    /// Interaction logic for SceneView.xaml
    /// </summary>
    [AvalonDockAnchorable(Strategy = AnchorableStrategy.Left, IsHidden = false, Title = "Scene", Size = 1200)]
    public partial class SceneView
    {
        private bool _lastVisible;
        private string _shadersPath = Path.GetFullPath(@"..\..\..\..\") + "SharkSpirit.Graphics\\Shaders";
        public SceneView()
        {
            InitializeComponent();

            InitializeSubscriptions();
        }

        private void InitializeSubscriptions()
        {
            Loaded += OnLoaded;
            SizeChanged += OnSizeChanged;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            if(DataContext is SceneViewModel)
                return;

            var windowHandle = (new WindowInteropHelper(Application.Current.MainWindow ?? throw new InvalidOperationException())).Handle;

            var sysConfig = new SystemConfiguration(new GraphicsConfigurationInfo
            {
                Height = ActualHeight,
                Width = ActualWidth,
                ShaderFilePath = _shadersPath
            });

            Console.WriteLine($"Path to shaders - {_shadersPath}");

            DataContext = new SceneViewModel(windowHandle, sysConfig);

            InteropImage.WindowOwner = windowHandle;
            InteropImage.OnRender = ((SceneViewModel)DataContext).OnRender;

            InteropImage.RequestRender();
        }

        private void OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            var dpiScale = 1.0; // default value for 96 dpi

            var sysConfig = new SystemConfiguration(new GraphicsConfigurationInfo
            {
                Height = ActualHeight,
                Width = ActualWidth,
                ShaderFilePath = _shadersPath
            });

            ((SceneViewModel) DataContext).SetNewConfiguration(sysConfig);

            // determine DPI
            // (as of .NET 4.6.1, this returns the DPI of the primary monitor, if you have several different DPIs)
            if (PresentationSource.FromVisual(this)?.CompositionTarget is HwndTarget hwndTarget)
            {
                dpiScale = hwndTarget.TransformToDevice.M11;
            }

            var surfWidth = (int)(ActualWidth < 0 ? 0 : Math.Ceiling(ActualWidth * dpiScale));
            var surfHeight = (int)(ActualHeight < 0 ? 0 : Math.Ceiling(ActualHeight * dpiScale));

            // notify the D3D11Image and the DxRendering component of the pixel size desired for the DirectX rendering.
            InteropImage.SetPixelSize(surfWidth, surfHeight);

            var isVisible = (surfWidth != 0 && surfHeight != 0);
            if (_lastVisible == isVisible) return;

            _lastVisible = isVisible;
            if (_lastVisible)
            {
                CompositionTarget.Rendering += OnCompositionTargetRendering;
            }
            else
            {
                CompositionTarget.Rendering -= OnCompositionTargetRendering;
            }
        }

        private void OnClosing(object sender, CancelEventArgs e)
        {
            CompositionTarget.Rendering -= OnCompositionTargetRendering;
        }

        private void OnCompositionTargetRendering(object sender, EventArgs e)
        {
            InteropImage.RequestRender();
        }
    }
}
